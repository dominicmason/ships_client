#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>
#include <sstream>
#include <chrono>
#include <thread>

#define GAMEDIR "../../gamedir"

//namespace fs = std::filesystem // Real Compilers
namespace fs = std::experimental::filesystem::v1; // VS 2015 :(

enum class BS {Board, Turn, Join};

class ivec2
{
public:
	int x, y;

	friend std::istream &operator>>(std::istream &in, ivec2 &out)
	{
		in >> out.y;
		in >> out.x;
		return in;
	}
	friend std::ostream &operator<<(std::ostream &out, const ivec2 &vec)
	{
		out << "(" << vec.y << "," << vec.x << ")";
		return out;
	}
};

void printFromFile(fs::path file)
{
	if (fs::exists(file))
	{
		std::ifstream reader(file, std::ios::in);
		if (reader.is_open())
			std::cout << std::string((std::istreambuf_iterator<char>(reader)), std::istreambuf_iterator<char>());
		else
			std::cout << "Unable to read " << file << std::endl;
	}
	else
		std::cout << "Unable to find " << file << std::endl;
}

void waitFor(fs::path file)
{
	while (!fs::exists(file))
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

void writeToFile(fs::path file, std::string str)
{
	std::ofstream out(file);
	if (out.is_open())
	{
		out << str;
		out.flush();
	}
	else
		std::cout << "Error opening " << file << std::endl;
}

fs::path getPath(BS fileType, int num)
{
	fs::path dir = GAMEDIR;
	std::string id = std::to_string(num);
	switch (fileType)
	{
	case BS::Board:
		return dir / fs::path("board" + id + ".bs");
		break;

	case BS::Turn:
		return dir / fs::path("turn" + id + ".bs");
		break;

	case BS::Join:
		return dir / fs::path("join" + id + ".bs");
		break;
	}
}

void requestToJoin()
{
	int i = 1;
	fs::path fileName = getPath(BS::Join, i);
	while (fs::exists(fileName))
	{
		fileName = getPath(BS::Join, ++i);
	}
	writeToFile(fileName, "");
}

int main()
{
	requestToJoin();
	
	printFromFile(getPath(BS::Turn, 1));
	printFromFile(getPath(BS::Board, 1));
    return 0;
}

